console.log("List-datatable.js");

let tblName = $("#tablePayment");


$(document).ready(function() {
    initDatatable(dataPaymentParse);
});


// Datatable Init
function initDatatable(dataParam) {
    return tblName.DataTable({

        //"sAjaxSource": "/api/payment/list",
        //"sAjaxDataProp": "",
        "order": [],
        "ordering": true,
        "searching": true,
        "pageLength": 10,
        "data": dataParam,
        "lengthChange": false,
        "paging": true,
        "destroy": true,
        "scrollX": true,
        "responsive": true,
        'autoWidth': false,
        "dom": 'Bfrtip',
        "buttons": [],
        "processing": true,
        "columns":[
            {
                "data": "invoiceNumber",
                "width": "5%"
            },
            {
                "data": "amount",
                "width": "5%"
            },
            {
                "data": "status",
                "width": "5%"
            },
            {
                "data": "payer",
               "width": "5%"

            },

        /*"aoColumns": [
            {
                "width": "2%",
                "mData": "id"
            },
            {
                "width": "6%",
                "mData": "name"
            },
            {
                "width": "6%",
                "mData": "address"
            },
            {
                "width": "6%",
                "mData": "phone"
            },
            {
                "width": "6%",
                "mData": "email"
            },
           */
            {
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "data": null,
                render: function (data, type, row) {
                    console.log(data.id);
                    let btnLink = "";
                    btnLink = btnLink + '<a class="btn btn-success" type="button"  href="/payment/print-invoice?invoiceNumber=' + data.invoiceNumber + '">Print</a>  ';
                    btnLink = btnLink + '<a class="btn btn-info" type="button"  href="/payment/edit-payment?idPayment=' + data.id + '">Edit</a>  ';
                    btnLink = btnLink + ' <a href="javascript:void(0)" type="button" data-id_delete="' + data.id + '" id="btn-delete" class="btn btn-danger">Delete</a>';
                    return btnLink;
                }
            },
        ],
        "buttons": [
                {
                    extend: 'copyHtml5',
                    text: 'Copy current page',
                    className: 'buttons-copy buttons-html5',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    className: 'buttons-csv buttons-html5',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    text: 'Xlsx',
                    className: 'buttons-xls buttons-html5',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: 'PDF',
                    className: 'buttons-pdf buttons-html5',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ]
    });
}

