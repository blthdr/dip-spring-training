console.log("List-datatable.js");

let tblName = $("#tableEmployee");


$(document).ready(function() {
    initDatatable(dataEmployeeParse);
});


// Datatable Init
function initDatatable(dataParam) {
    return tblName.DataTable({

        //"sAjaxSource": "/api/employee/list",
        //"sAjaxDataProp": "",
        "order": [],
        "ordering": true,
        "searching": true,
        "pageLength": 10,
        "data": dataParam,
        "lengthChange": false,
        "paging": true,
        "destroy": true,
        "scrollX": true,
        "responsive": true,
        'autoWidth': false,
        "dom": 'Bfrtip',
        "buttons": [],
        "processing": true,
        "columns":[
            {
                "data": "id",
                "width": "5%"
            },
            {
                "data": "name",
                "width": "5%"
            },
            {
                "data": "email",
                "width": "5%"
            },
            {
                "data": "phone",
               "width": "5%"

            },
            {
                "data": "address",
                "width": "5%"
            },

        /*"aoColumns": [
            {
                "width": "2%",
                "mData": "id"
            },
            {
                "width": "6%",
                "mData": "name"
            },
            {
                "width": "6%",
                "mData": "address"
            },
            {
                "width": "6%",
                "mData": "phone"
            },
            {
                "width": "6%",
                "mData": "email"
            },
           */
            {
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "data": null,
                render: function (data, type, row) {
                    console.log(data.id);
                    let btnLink = "";
                    btnLink = btnLink + '<a class="btn btn-info" type="button"  href="/employee/edit-employee?idEmployee=' + data.id + '">Edit</a>  ';
                    btnLink = btnLink + ' <a href="javascript:void(0)" type="button" data-id_delete="' + data.id + '" id="btn-delete" class="btn btn-danger">Delete</a>';
                    return btnLink;
                }
            },
        ],
    });
}

