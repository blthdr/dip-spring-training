console.log("List-datatable.js");

let tblName = $("#tableProduct");


$(document).ready(function() {
    initDatatable(dataProductParse);
});


// Datatable Init
function initDatatable(dataParam) {
    return tblName.DataTable({

        //"sAjaxSource": "/api/payment/list",
        //"sAjaxDataProp": "",
        "order": [],
        "ordering": true,
        "searching": true,
        "pageLength": 10,
        "data": dataParam,
        "lengthChange": false,
        "paging": true,
        "destroy": true,
        "scrollX": true,
        "responsive": true,
        'autoWidth': false,
        "dom": 'Bfrtip',
        "buttons": [],
        "processing": true,
        "columns":[
            {
                "data": "name",
                "width": "5%"
            },
            {
                "data": "price",
                "width": "5%"
            },
            {
                "data": "qty",
                "width": "5%"
            },
            {
                "data": "sku",
               "width": "5%"

            },

        /*"aoColumns": [
            {
                "width": "2%",
                "mData": "id"
            },
            {
                "width": "6%",
                "mData": "name"
            },
            {
                "width": "6%",
                "mData": "address"
            },
            {
                "width": "6%",
                "mData": "phone"
            },
            {
                "width": "6%",
                "mData": "email"
            },
           */
            {
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "data": null,
                render: function (data, type, row) {
                    console.log(data.id);
                    let btnLink = "";
                    btnLink = btnLink + '<a class="btn btn-success" type="button"  href="/product/print-invoice?invoiceNumber=' + data.invoiceNumber + '">Print</a>  ';
                    btnLink = btnLink + '<a class="btn btn-info" type="button"  href="/product/edit-payment?idPayment=' + data.id + '">Edit</a>  ';
                    btnLink = btnLink + ' <a href="javascript:void(0)" type="button" data-id_delete="' + data.id + '" id="btn-delete" class="btn btn-danger">Delete</a>';
                    return btnLink;
                }
            },
        ],
        "buttons": [
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    className: 'btn btn-info',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    text: 'Xlsx',
                    className: 'btn btn-warning',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: 'PDF',
                    className: 'btn btn-success',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ]
    });
}

