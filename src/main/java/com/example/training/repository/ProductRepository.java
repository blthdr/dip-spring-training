package com.example.training.repository;

import com.example.training.models.Product;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JdbcHelper;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class ProductRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;
    SimpleJdbcCall simpleJdbcCall;

    public List<Product> listProduct(Product dataParam){

        // Calling Stored Procedure to get all user list
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PRODUCT_LIST")
                .mapTo(Product.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataParam.getId())
                .addValue("P_SKU", dataParam.getSku())
                .addValue("P_NAME", dataParam.getName());

        //Storeprocedure execution
        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Ger Result Value To Object
        List<Product> userList = (List<Product>) resultSp.get(InformationConstant.REF_CURSOR_RECORDSET);


        /*
        System.out.println("Isi Employee");
        System.out.println(userList.get(0).getName());
        System.out.println(userList.get(0).getAddress());
        */
        return userList;
    }

    public ResponseSave insertProduct(Product dataForSave) {

        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PRODUCT_INSERT")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_NAME", dataForSave.getName())
                .addValue("P_QTY", dataForSave.getQty())
                .addValue("P_PRICE", dataForSave.getPrice())
                .addValue("P_SKU", dataForSave.getSku())
                .addValue("P_CREATEBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }


    public ResponseSave updateProduct(Product dataForSave) {

        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PRODUCT_UPDATE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataForSave.getId())
                .addValue("P_NAME", dataForSave.getName())
                .addValue("P_PRICE", dataForSave.getPrice())
                .addValue("P_QTY", dataForSave.getQty())
                .addValue("P_SKU", dataForSave.getSku())
                .addValue("P_EDITBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }


    public ResponseSave deleteProduct(Product dataForSave) {

        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PRODUCT_DELETE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataForSave.getId());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }
}
