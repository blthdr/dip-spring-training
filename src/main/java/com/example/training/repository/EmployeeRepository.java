package com.example.training.repository;

import com.example.training.controllers.EmployeeController;
import com.example.training.models.Employee;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JdbcHelper;
import com.example.training.viewmodel.EmployeeAddress;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class EmployeeRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;
    SimpleJdbcCall simpleJdbcCall;

    public List<Employee> listEmployee(Employee dataParam){

        // Calling Stored Procedure to get all user list
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_LIST")
                .mapTo(Employee.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataParam.getId())
                .addValue("P_Name", dataParam.getName());

        //Storeprocedure execution
        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Ger Result Value To Object
        List<Employee> userList = (List<Employee>) resultSp.get(InformationConstant.REF_CURSOR_RECORDSET);
        /*
        System.out.println("Isi Employee");
        System.out.println(userList.get(0).getName());
        System.out.println(userList.get(0).getAddress());
        */
        return userList;
    }

    public ResponseSave insertEmployee(Employee dataForSave) {

        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_INSERT")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_NAME", dataForSave.getName())
                .addValue("P_EMAIL", dataForSave.getEmail())
                .addValue("P_PHONE", dataForSave.getPhone())
                .addValue("P_ADDRESS", dataForSave.getAddress())
                .addValue("P_CREATEBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }


    public ResponseSave updateEmployee(Employee dataForSave) {

        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_UPDATE")
                .spName("SP_EMPLOYEE_UPDATE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_NAME", dataForSave.getName())
                .addValue("P_EMAIL", dataForSave.getEmail())
                .addValue("P_PHONE", dataForSave.getPhone())
                .addValue("P_ADDRESS", dataForSave.getAddress())
                .addValue("P_UPDATEBY", dataForSave.getCreateBy())
                .addValue("P_ID", dataForSave.getId());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }


    public ResponseSave deleteEmployee(Employee dataForSave) {

        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_DELETE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataForSave.getId());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }
}
