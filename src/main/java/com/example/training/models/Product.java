package com.example.training.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Product {
    public Integer id;
    public String name;
    public Integer price;
    public Integer qty;
    public String sku;
    public String createBy;
}
