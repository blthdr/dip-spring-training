package com.example.training.models;


import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.AllArgsConstructor;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee {
    public Integer id;
    public String name;
    public String email;
    public String phone;
    public String address;
    public Timestamp createDate;
    public String createBy;

}
