package com.example.training.viewmodel;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AjaxResponseBody {

    public String statusCode;
    public String massage;

}
