package com.example.training.viewmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class EmployeeAddress {
    public String warnaRumah;
    public String luasRumah;
    public String jumlahMobil;

}
