package com.example.training.utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JsonHelper {

    public static String toJasonString(Object o){
        GsonBuilder gsonBuiler = new GsonBuilder();
        gsonBuiler.serializeNulls();
        Gson gson = gsonBuiler.disableHtmlEscaping().create();

        return gson.toJson(o);
    }
}
