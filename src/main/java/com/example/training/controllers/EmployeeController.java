package com.example.training.controllers;

import com.example.training.models.Employee;
import com.example.training.services.EmployeeService;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping(value = "/list")
    //public ResponseEntity<?> listEmployee(){
    public String listEmployee(Model model){
        Employee employeeParam = new Employee();

        List<Employee> data = employeeService.getAllEmployee(employeeParam);

        //System.out.println(data);

        //return ResponseEntity.ok().body(data);

        model.addAttribute("dataEmployee", data);
        return "employee/list";
        //return "dashboard";
    }

    @GetMapping(value = ("/list-role"))
    public String listRoleEmployee(Model model){

        String titlePage = "Employee Role";
        String username = "Admin";

        model.addAttribute("title", titlePage);
        model.addAttribute("user", username);

        return "employee/list-role";
    }

    @GetMapping(value=("/form-employee"))
    public String newEmployee(Model model){
        String title = "Add Employee" + InformationConstant.websiteTitle;
        String titleBar = "Add Employee";
        String titleContent = "Insert Data Employee";


        model.addAttribute("title",title);
        model.addAttribute("titleBar", titleBar);
        model.addAttribute("titleContent", titleContent);



        model.addAttribute("dataEmployee", new Employee());

        return "employee/form-employee";
    }

    @GetMapping(value=("/edit-employee"))
    public String editEmployee(@RequestParam String idEmployee, Model model){
        String title = "Add Employee" + InformationConstant.websiteTitle;
        String titleBar = "Add Employee";
        String titleContent = "Insert Data Employee";





        // Set Parameter for filtering
        Employee employeeParam = new Employee();
        employeeParam.setId(Integer.parseInt(idEmployee));

        // Get Data from Service
        Employee data = employeeService.getAllEmployee(employeeParam).get(0);

        // Set Model for View Attribiute
        model.addAttribute("dataEmployee", data);
        // Attribute for fragments
        model.addAttribute("title",title);
        model.addAttribute("titleBar", titleBar);
        model.addAttribute("titleContent", titleContent);

        return "employee/form-employee";

    }


    @GetMapping(value = "/list-datatable")
    //public ResponseEntity<?> listEmployee(){
    public String listEmployeeDataTables(Model model){

        Employee employeeParam = new Employee();

        List<Employee> data = employeeService.getAllEmployee(employeeParam);

        String title = "Employee" + InformationConstant.websiteTitle;
        String titleBar = "Employee";
        String titleContent = "Employee Data Tables";


        model.addAttribute("title",title);
        model.addAttribute("titleBar", titleBar);
        model.addAttribute("titleContent", titleContent);

        model.addAttribute("dataEmployee", JsonHelper.toJasonString(data));
        //Employee employeeParam = new Employee();

        //List<Employee> data = employeeService.getAllEmployee(employeeParam);

        //System.out.println(data);

        //return ResponseEntity.ok().body(data);

        //model.addAttribute("dataEmployee", data);
        return "employee/list-datatable";
        //return "dashboard";
    }
}
