package com.example.training.controllers;

import com.example.training.models.Payment;
import com.example.training.services.PaymentService;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.util.List;

@Controller
@RequestMapping(value = "/payment")
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    @GetMapping(value="/list")
    public String paymentList(Model model) {

        String title = "Payment" + InformationConstant.websiteTitle;
        String titleBar = "Add Payment";
        String titleContent = "List Payment";


        model.addAttribute("title",title);
        model.addAttribute("titleBar", titleBar);
        model.addAttribute("titleContent", titleContent);

        Payment paymentParam = new Payment();

        List<Payment> data = paymentService.getAllPayment(paymentParam);

        System.out.println(data.get(0).getId());
        model.addAttribute("dataPayment", JsonHelper.toJasonString(data));
//
//        // set parameter for sample
//        Payment payment = new Payment();
//        payment.setId("1");
//        // call method document Payment Stream
//        paymentService.documentPaymentStream(payment);

        return "payment/list";
    }
/*
    @GetMapping(value = "/print-invoice")
    public ResponseEntity printInvoice() throws Exception{
        ByteArrayInputStream in = null;

        // set parameter for sample
        Payment payment = new Payment();
        payment.setId("1");
        in = paymentService.documentPaymentStream(payment);
        // call method document Payment Stream
        paymentService.documentPaymentStream(payment);


        HttpHeaders headers = new HttpHeaders();
        String headerValue = "attachment; filename=" + "Surat-Tagihan" + ".pdf";
        headers.add("Content-Disposition", headerValue);

        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.setContentType(MediaType.parseMediaType("application/pdf"));

        return ResponseEntity.ok()
                .headers(headers)
                .body(new InputStreamResource(in));


    }
*/
    @GetMapping(value = "/print-invoice")
    public ResponseEntity printInvoice(@RequestParam String invoiceNumber) throws Exception{
        ByteArrayInputStream in = null;

        // set parameter for sample
        Payment payment = new Payment();
       // System.out.println("Invoice Number : " + invoiceNumber);
        payment.setInvoiceNumber(invoiceNumber);

        // call method document Payment Stream
        in = paymentService.documentPaymentStream(payment);


        HttpHeaders headers = new HttpHeaders();
        String headerValue = "attachment; filename=" + "Surat-Tagihan" + ".pdf";
        headers.add("Content-Disposition", headerValue);

        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.setContentType(MediaType.parseMediaType("application/pdf"));

        return ResponseEntity.ok()
                .headers(headers)
                .body(new InputStreamResource(in));


    }
}
