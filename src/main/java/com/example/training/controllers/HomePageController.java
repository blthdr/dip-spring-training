package com.example.training.controllers;

import com.example.training.utilities.InformationConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
//@RequestMapping(value="/dashboard")
public class HomePageController {

    @RequestMapping(value="/dashboard", method= {RequestMethod.GET, RequestMethod.POST})
    //@GetMapping(value="")
    public String dashboard(Model model){
        String pageTitle = "Dashboard" + InformationConstant.websiteTitle;

        model.addAttribute("username", "Ivan");
        model.addAttribute("title", pageTitle);

        return "dashboard";
    }
    @RequestMapping(value="/detail-visitor", method= {RequestMethod.GET, RequestMethod.POST})
    //@GetMapping(value="/detail-visitor")
    public ResponseEntity dashboardDetailVisitor(){
        return ResponseEntity.ok("This is Page Detail Visitor");
    }

}
