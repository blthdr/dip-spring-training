package com.example.training.controllers;

import com.example.training.utilities.InformationConstant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
    @RequestMapping(value={"/", "/login"}, method= {RequestMethod.GET, RequestMethod.POST})
    public String login(Model model){

        return "login";
    }


}
