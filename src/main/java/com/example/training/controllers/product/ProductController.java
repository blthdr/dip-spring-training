package com.example.training.controllers.product;

import com.example.training.models.Product;
import com.example.training.services.ProductService;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value="/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(value="/list")
    public String list(Model model){
        String title = "Product" + InformationConstant.websiteTitle;
        String titleBar = "Add Product";
        String titleContent = "Insert Data Product";


        model.addAttribute("title",title);
        model.addAttribute("titleBar", titleBar);
        model.addAttribute("titleContent", titleContent);

        Product productParam = new Product();
        List<Product> data = productService.getAllProduct(productParam);

        System.out.println(data);
        model.addAttribute("dataProduct", JsonHelper.toJasonString(data));

        return "product/list";
    }

    @GetMapping(value="/form-product")
    public String save(Model model){
        String title = "Product" + InformationConstant.websiteTitle;
        String titleBar = "Add Product";
        String titleContent = "Insert Data Product";


        model.addAttribute("title",title);
        model.addAttribute("titleBar", titleBar);
        model.addAttribute("titleContent", titleContent);



        model.addAttribute("dataProduct", new Product());
        return "product/form-product";
    }

    @GetMapping(value="/edit-product")
    public String edit(@RequestParam String idProduct, Model model){
        String title = "Product" + InformationConstant.websiteTitle;
        String titleBar = "Edit Product";
        String titleContent = "Edit Data Product";

        model.addAttribute("title",title);
        model.addAttribute("titleBar", titleBar);
        model.addAttribute("titleContent", titleContent);



        Product productParam = new Product();
        productParam.setId(Integer.parseInt(idProduct));
        Product data = productService.getAllProduct(productParam).get(0);

        model.addAttribute("dataProduct", data);

        return "product/form-product";
    }
}

