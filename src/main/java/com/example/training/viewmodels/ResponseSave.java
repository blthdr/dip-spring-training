package com.example.training.viewmodels;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseSave {
    public String id;
    public String errorMsg;
}
