package com.example.training.api.payment;

import com.example.training.models.Employee;
import com.example.training.models.Payment;
import com.example.training.services.PaymentService;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JsonHelper;
import com.example.training.viewmodel.AjaxResponseBody;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.training.viewmodels.ResponseSave;

import java.util.List;

@RestController
@RequestMapping(value=("/api/payment"))
public class paymentApi {

    @Autowired
    PaymentService paymentService;

    @GetMapping(value=("/list"))
    public List<Payment> listPaymentApi(){return paymentService.getAllPayment(new Payment()); }
}
